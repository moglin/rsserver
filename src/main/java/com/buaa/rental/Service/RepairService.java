package com.buaa.rental.Service;

import com.buaa.rental.Dao.Application;
import com.buaa.rental.Dao.Repair;
import com.buaa.rental.Dao.User;
import com.fasterxml.jackson.databind.deser.CreatorProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class RepairService {

    @Autowired
    MongoTemplate mongoTemplate;

    public long addRepair(long aid, Repair.Message message, String title) {

        Repair repair = new Repair();
        repair.aid = aid;
        repair.timeline = new ArrayList<>();
        repair.timeline.add(message);
        repair.state = Repair.RepairState.unset;
        repair.title = title;
        mongoTemplate.insert(repair);

        return repair.id;
    }

    public Repair getRepairById(long rid) {
        Query q = new Query(Criteria.where("id").is(rid));
        return mongoTemplate.findOne(q, Repair.class);
    }

    public List<Repair> getRepairByApplication(long aid) {
        Query q = new Query(Criteria.where("aid").is(aid));
        return mongoTemplate.find(q, Repair.class);
    }

    public List<Repair> getRepairByClient(long uid) {
        Query q1 = new Query(Criteria.where("uid").is(uid));
        Query q2 = new Query(Criteria.where("aid").in(
                mongoTemplate.findDistinct(q1,"id",Application.class,Long.class)));
        return mongoTemplate.find(q2, Repair.class);
    }

    public List<Repair> getRepairByWorker(long uid) {
        Query q = new Query(Criteria.where("workerId").is(uid));
        return mongoTemplate.find(q, Repair.class);
    }

    public void assignWorker(long rid, long uid){
        Query q1 = new Query(Criteria.where("id").is(rid));
        Update u1 = Update.update("workerId", uid);
        u1.set("state", Repair.RepairState.set);
        mongoTemplate.updateFirst(q1, u1, Repair.class);
    }

    public void addMessage(Repair repair, Repair.Message message) {
        Query q1 = new Query(Criteria.where("id").is(repair.id));
        message.createDate = new Date();
        repair.timeline.add(message);
        Update u1 = Update.update("timeline", repair.timeline);
        mongoTemplate.updateFirst(q1, u1, Repair.class);
    }

    public void changeStatus(long id, Repair.RepairState tos) {
        Query q1 = new Query(Criteria.where("id").is(id));
        Update u1 = Update.update("state", tos);
        mongoTemplate.updateFirst(q1, u1, Repair.class);
    }

    public List<Repair> getAllRepairs() {
        return mongoTemplate.findAll(Repair.class);
    }

    public void rate(long id, Repair.Message message,Repair.Opinion opinion){
        Query q1 = new Query(Criteria.where("id").is(id));
        Update u1 = Update.update("opinion", opinion);
        u1.set("opinionContent",message);
        u1.set("state", Repair.RepairState.rated);
        mongoTemplate.updateFirst(q1,u1,Repair.class);

    }
}
