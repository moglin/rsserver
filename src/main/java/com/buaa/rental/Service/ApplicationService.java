package com.buaa.rental.Service;

import com.buaa.rental.Dao.Application;
import com.buaa.rental.Dao.Application.Status;

import java.awt.desktop.QuitResponse;
import java.util.Date;
import java.util.List;

import com.buaa.rental.Dao.Repair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

@Service
public class ApplicationService
{

	@Autowired
	public MongoTemplate mongoTemplate;

	public Application insert(Application app)
	{
		mongoTemplate.insert(app);
		return app;
	}

	public Application findById(long id)
	{
		Query q1 = new Query(Criteria.where("id").is(id));
		return mongoTemplate.findOne(q1, Application.class);
	}

	public List<Application> findByStatus(Status status)
	{
		Query q1 = new Query(Criteria.where("status").is(status));
		return mongoTemplate.find(q1, Application.class);
	}

	public void changeStatus(long id, Status tos)
	{
		Query q1 = new Query(Criteria.where("id").is(id));
		Update u1 = Update.update("status", tos);
		mongoTemplate.updateFirst(q1, u1, Application.class);
	}

	public void updateRenewalTime(long id, Date date)
	{
		Query q1 = new Query(Criteria.where("id").is(id));
		Update u1 = Update.update("renewalTime", date);
		mongoTemplate.updateFirst(q1, u1, Application.class);
	}

	public void updateEndTime(long id, Date date)
	{
		Query q1 = new Query(Criteria.where("id").is(id));
		Update u1 = Update.update("endTime", date);
		mongoTemplate.updateFirst(q1, u1, Application.class);
	}

	public List<Application> findByUser(long uid)
	{
		Query q = new Query(Criteria.where("uid").is(uid));
		return mongoTemplate.find(q, Application.class);
	}

	public List<Application> findAll()
	{
		return mongoTemplate.findAll(Application.class);
	}

	public void setContract(long aid,String contract){
		Query q = new Query(Criteria.where("id").is(aid));
		Update u = Update.update("contract",contract);
		mongoTemplate.updateFirst(q,u, Application.class);
	}

}
