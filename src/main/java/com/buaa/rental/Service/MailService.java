package com.buaa.rental.Service;

import java.util.Date;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class MailService
{

	@Autowired
	private JavaMailSender mailSender;

	@Value("${spring.mail.self.username}")
	private String sendUser;

	public void sendMessage(String title, String msg,
			String receiveMail) throws Exception
	{
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper message = new MimeMessageHelper(mimeMessage);

		message.setFrom(sendUser);

		message.setTo("buaase@163.com");
		message.setSubject(title);
		message.setSentDate(new Date());
		message.setText(msg);
		mailSender.send(mimeMessage);
		message.setTo(receiveMail);
		mailSender.send(mimeMessage);
	}
}