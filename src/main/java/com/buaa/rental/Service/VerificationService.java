package com.buaa.rental.Service;

import com.buaa.rental.Dao.Verification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class VerificationService
{

	@Autowired
	MongoTemplate mongoTemplate;

	public String getCode(String email) throws Exception
	{
		Query q = new Query(Criteria.where("email").is(email));
		Verification v = mongoTemplate.findOne(q, Verification.class);
		return v.code;
	}

	public void updateCode(String email,String code) throws Exception
	{
		Query q = new Query(Criteria.where("email").is(email));
		Verification v = mongoTemplate.findOne(q, Verification.class);
		if(v == null)
		{
			v = new Verification();
			v.email = email;
		}
		v.code = code;
		mongoTemplate.save(v);
	}
}
