package com.buaa.rental.Util.MyAnnotations;


import com.buaa.rental.Dao.User.Role;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Authorization
{

	public Role role() default Role.none;
}