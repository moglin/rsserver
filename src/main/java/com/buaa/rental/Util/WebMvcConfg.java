package com.buaa.rental.Util;

import com.buaa.rental.Util.MyAnnotations.CurrentUserMethodArgumentResolver;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfg implements WebMvcConfigurer
{

	@Override
	public void addInterceptors(InterceptorRegistry registry)
	{
		//注册拦截器 拦截规则
		//多个拦截器时 以此添加 执行顺序按添加顺序
		registry.addInterceptor(getHandlerInterceptor()).addPathPatterns("/**");
	}

	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers)
	{
		resolvers.add(new CurrentUserMethodArgumentResolver());
	}

	@Bean
	public AuthorizedInterceptor getHandlerInterceptor()
	{
		return new AuthorizedInterceptor();
	}
}