package com.buaa.rental.Util;

public class CodeException extends Exception
{

	public String code;

	public CodeException(String code, String msg)
	{
		super(msg);
		this.code = code;

	}
}
