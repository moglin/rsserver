package com.buaa.rental.Util;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

public class DataFormat
{

	public static String bindingErrorsFormat(BindingResult res)
	{
		StringBuilder sb = new StringBuilder();
		for (ObjectError objectError : res.getAllErrors())
		{
			sb.append(((FieldError) objectError).getField()).append(" : ")
					.append(objectError.getDefaultMessage()).append("\n");
		}
		return sb.toString();
	}

}
