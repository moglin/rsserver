package com.buaa.rental.Dao;

import com.buaa.rental.Util.MyAnnotations.AutoIncrement;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.validation.constraints.*;
import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "user")
public class User implements Serializable
{

	private static final long serialVersionUID = 6754006769687927477L;

	/**
	 * 用户角色，管理员、普通用户、维修工人，管理员不可注册
	 **/
	public enum Role
	{
		none,
		admin,
		user,
		worker
	}

	@Id
	@JsonIgnore
	public String id;
	@AutoIncrement(baseNum = 123456)
	public long uid;

	@Indexed(unique = true)
	@Email(message = "需要合法邮箱")
	public String email;

	@Size(min = 3, max = 20, message = "用户名长度应该在3-20个字符")
	public String name;
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@Size(min = 6, max = 20, message = "密码长度应该在3-20个字符")
	public String psw;

	public Role role;
	@CreatedDate
	public Date registCreate;

	public String portraitPath;

	public String verificationCode;

	public int age;
	public String province;
	public String district;
	public String phoneNum;
	@Size(max = 500, message = "用户描述不能超过500个字符")
	public String personIntro;
	public String avatar;

	/*public Map<String, Object> getUserInfo()
	{
		Map<String, Object> m = new HashMap<>();
		m.put("id", uid);
		m.put("name", name);
		m.put("email", email);
		m.put("role", role);
		return m;
	}*/
}
