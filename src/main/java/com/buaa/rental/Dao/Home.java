package com.buaa.rental.Dao;

import com.buaa.rental.Util.MyAnnotations.AutoIncrement;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.*;

@Document(collection = "home")
public class Home implements Serializable
{

	private static final long serialVersionUID = 2878451870439714162L;

	/**
	 * 租赁方式: 长租短租
	 **/
	public enum Rentalmethod
	{
		longr,
		shortr
	}

	public enum RoomType
	{
		singleroom,
		doubleroom,
		quadrupleroom
	}

	public enum RoomStatus
	{
		enable,
		applied,
		leased,
		stop
	}

	@Id
	@AutoIncrement
	public long id;

	@CreatedDate
	public Date registCreate;


	@NotBlank(message = "标题不能为空")
	@Size(max = 40, message = "标题长度不超过40个字")
	public String title;
	@Size(min = 10, max = 500, message = "描述应在10-100个字符之间")
	public String description;

	public int floor;
	@NotBlank(message = "地址不能为空")
	public String address;
	public double longitude;
	public double latitude;

	public double area;

	public String layout; // 户型

	public List<String> tags;

	public List<String> photos;

	//@NotBlank(message = "租房价格不能为空")
	public int price;

	public RoomStatus roomStatus;
	public Rentalmethod rentalmethod;
}
