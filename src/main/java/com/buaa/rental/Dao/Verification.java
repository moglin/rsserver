package com.buaa.rental.Dao;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Verification")
public class Verification
{
	@Id
	public String id;

	@Indexed(unique = true)
	public String email;
	public String code;
}
