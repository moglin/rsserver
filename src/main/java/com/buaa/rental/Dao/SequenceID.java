package com.buaa.rental.Dao;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "sequence")
public class SequenceID
{
	@Id
	private String id;
	private long seqId;
	private String collName;

	public long getSeqId()
	{
		return seqId;
	}
}