package com.buaa.rental.Dao;

import com.buaa.rental.Util.MyAnnotations.AutoIncrement;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Document(collection = "repair")
public class Repair implements Serializable {

    private static final long serialVersionUID = 3752080405307390574L;


    public enum RepairState{
        unset,
        set,
        closed,
        rated
    }

    public enum Opinion{
        good,
        medium,
        bad
    }

    public static class Message implements Serializable {


        private static final long serialVersionUID = 4406980780743028123L;

        @NotNull
        public long uid;

        @Size(min = 10, message = "描述不少于10个字符")
        public String text;

        public List<String> photos;

        public Date createDate;
    }

    @Id
    @AutoIncrement
    public long id;

    @CreatedDate
    public Date repairCreate;

    public long aid;

    public String title;

    public List<Message> timeline;

    public RepairState state;

    public Message opinionContent;
    public Opinion opinion;

    public long workerId;

}
