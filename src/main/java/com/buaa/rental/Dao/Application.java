package com.buaa.rental.Dao;

import com.buaa.rental.Util.MyAnnotations.AutoIncrement;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "application")
public class Application
{

	public enum Status
	{
		apply,
		pass,
		refuse,
		success,
		renewal
	}

	public enum Rentalmethod
	{
		longr,
		shortr
	}

	@Id
	@AutoIncrement
	public long id;

	@Indexed
	public long uid;

	@Indexed
	@Min(value = 1,message = "房屋不能为空")
	public long hid;

	public Date beginTime;
	public Date endTime;

	public Date renewalTime;
	public Rentalmethod rentalmethod;
	public Status status;

	public String contract;

}
