package com.buaa.rental.AdminController;

import com.buaa.rental.Dao.Application;
import com.buaa.rental.Dao.Application.Rentalmethod;
import com.buaa.rental.Dao.Application.Status;
import com.buaa.rental.Dao.CommonRep;
import com.buaa.rental.Dao.Home;
import com.buaa.rental.Dao.Home.RoomStatus;
import com.buaa.rental.Dao.User;
import com.buaa.rental.Dao.User.Role;
import com.buaa.rental.Service.ApplicationService;
import com.buaa.rental.Service.HomeService;
import com.buaa.rental.Service.RepairService;
import com.buaa.rental.Util.CodeException;
import com.buaa.rental.Util.DataFormat;
import com.buaa.rental.Util.MyAnnotations.Authorization;
import java.util.Date;
import java.util.List;
import javax.validation.Valid;

import com.buaa.rental.Util.MyAnnotations.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("api/admin/home/")
public class AdminHomeController
{

	@Autowired
	private HomeService homeService;
	@Autowired
	private ApplicationService applicationService;
	@Autowired
	private RepairService repairService;

	@Authorization(role = Role.admin)
	@PostMapping("addHome")
	public CommonRep addHome(@RequestBody @Valid Home home, BindingResult res) throws CodeException
	{
		if (res.hasErrors())
		{
			throw new CodeException("101", DataFormat.bindingErrorsFormat(res));
		}
		home.roomStatus = RoomStatus.enable;
		if (home.rentalmethod == null || home.layout == null)
		{
			throw new CodeException("102", "类型不能为空");
		}
		home = homeService.addHome(home);
		if (home == null)
		{
			throw new CodeException("102", "添加房源失败");
		}
		return new CommonRep();
	}

	@Authorization(role = Role.admin)
	@GetMapping("getApplicationToSolve")
	public CommonRep getApplicationToSolve()
	{
		CommonRep rep = new CommonRep();
		List<Application> apps = applicationService.findByStatus(Status.apply);
		apps.addAll(applicationService.findByStatus(Status.renewal));
		rep.setData(apps);
		return rep;
	}

	@Authorization(role = Role.admin)
	@PostMapping("updateApplicationStatus")
	public CommonRep updateApplicationStatus(long aid, Status tos) throws CodeException
	{
		CommonRep rep = new CommonRep();
		Application app = applicationService.findById(aid);
		Home home = homeService.findById(app.hid);
		if (app.status == Status.renewal)
		{
			if (tos != Status.refuse)
			{
				applicationService.updateEndTime(aid, app.renewalTime);
			} else
			{
				if (home.roomStatus == RoomStatus.applied)
				{
					homeService.changeStatus(home.id, RoomStatus.enable);
				}
			}
			applicationService.changeStatus(aid, Status.success);
			return rep;
		} else if (tos == Status.pass)
		{
			if (home.roomStatus != RoomStatus.applied || app.status != Status.apply)
			{
				throw new CodeException("102", "房源参数错误");
			}
			homeService.changeStatus(home.id, RoomStatus.leased);
		}
		if (tos == Status.refuse)
		{
			if (home.roomStatus == RoomStatus.applied)
			{
				homeService.changeStatus(home.id, RoomStatus.enable);
			}
		}
		applicationService.changeStatus(aid, tos);
		return rep;
	}

	@Authorization(role = Role.admin)
	@PostMapping("updateHomeStatus")
	public CommonRep updateHomeStatus(long hid, RoomStatus tos) throws CodeException
	{
		homeService.changeStatus(hid, tos);
		return new CommonRep();
	}

	@Authorization(role = Role.admin)
	@PostMapping("updateContract")
	public CommonRep updateContract(long aid, String file) throws CodeException
	{
		applicationService.setContract(aid,file);
		return new CommonRep();
	}

	@Authorization(role = Role.admin)
	@GetMapping("getAllApplication")
	public CommonRep getAllApp(@CurrentUser User user)
	{
		CommonRep rep = new CommonRep();
		List<Application> apps = applicationService.findAll();
		rep.setData(apps);
		return rep;
	}

	@Scheduled(cron = " 0 0 13 * * ?")
	public void updateHomeStatusTiming()
	{
		List<Application> apps = applicationService.findByStatus(Status.success);
		Date now = new Date();
		for (Application a : apps)
		{
			if (a.endTime.compareTo(now) <= 0)
			{
				homeService.changeStatus(a.hid, RoomStatus.enable);
			}
		}
		List<Application> rapps = applicationService.findByStatus(Status.renewal);
		for (Application a : rapps)
		{
			if (a.endTime.compareTo(now) <= 0)
			{
				homeService.changeStatus(a.hid, RoomStatus.applied);
			}
		}
	}

}
