package com.buaa.rental.AdminController;


import com.buaa.rental.Dao.CommonRep;
import com.buaa.rental.Dao.Repair;
import com.buaa.rental.Dao.User;
import com.buaa.rental.Service.ApplicationService;
import com.buaa.rental.Service.HomeService;
import com.buaa.rental.Service.RepairService;
import com.buaa.rental.Util.CodeException;
import com.buaa.rental.Util.MyAnnotations.Authorization;
import com.buaa.rental.Util.MyAnnotations.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/admin/home/")
public class AdminRepairController {
    @Autowired
    private HomeService homeService;
    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private RepairService repairService;

    @Authorization(role = User.Role.admin)
    @GetMapping("getAllRepair")
    public CommonRep getRepairs(){
        CommonRep rep = new CommonRep();
        rep.setData(repairService.getAllRepairs());
        return rep;
    }

    @Authorization(role = User.Role.admin)
    @PostMapping("assignWorker")
    public CommonRep assignWorker(@RequestBody Map<String, String> map) {
        long rid = Integer.parseInt(map.get("rid"));
        long wid = Integer.parseInt(map.get("wid"));
        repairService.assignWorker(rid,wid);
        return new CommonRep();
    }

    @Authorization(role = User.Role.admin)
    @PostMapping("addMessage")
    public CommonRep addMessage(@CurrentUser User user, @RequestBody Map<String, Object> map){
        long rid = Integer.parseInt((String) map.get("rid"));
        Repair repair = repairService.getRepairById(rid);
        Repair.Message message = new Repair.Message();
        message.uid = user.uid;
        message.text = (String) map.get("text");
        message.photos = (List<String>) map.get("photos");
        repairService.addMessage(repair, message);
        return new CommonRep();
    }

    @Authorization(role = User.Role.admin)
    @PostMapping("close")
    public CommonRep closeRepair(@CurrentUser User user, @RequestBody Map<String, String> map) throws Exception {
        long rid = Integer.parseInt(map.get("rid"));
        repairService.changeStatus(rid, Repair.RepairState.closed);
        return new CommonRep();
    }

}
