package com.buaa.rental;

import com.buaa.rental.Dao.CommonRep;
import com.buaa.rental.Dao.User;
import com.buaa.rental.Dao.User.Role;
import com.buaa.rental.Util.MyAnnotations.Authorization;
import com.buaa.rental.Util.MyAnnotations.CurrentUser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Hello
{

	@RequestMapping("/")
	public String hello()
	{
		return "hello world";
	}

	@Authorization
	@RequestMapping("/author")
	public CommonRep author(@CurrentUser User user)
	{
		CommonRep rep = new CommonRep();
		rep.setData(user.role);
		return rep;
	}

	@Authorization(role = Role.admin)
	@GetMapping("/adminAuthor")
	public CommonRep adminAuthor(@CurrentUser User user)
	{
		return new CommonRep();
	}
}
