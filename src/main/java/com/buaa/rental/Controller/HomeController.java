package com.buaa.rental.Controller;

import com.buaa.rental.Dao.Application;
import com.buaa.rental.Dao.Application.Rentalmethod;
import com.buaa.rental.Dao.Application.Status;
import com.buaa.rental.Dao.CommonRep;
import com.buaa.rental.Dao.Home;
import com.buaa.rental.Dao.Home.RoomStatus;
import com.buaa.rental.Dao.User;
import com.buaa.rental.Service.ApplicationService;
import com.buaa.rental.Service.HomeService;
import com.buaa.rental.Util.CodeException;
import com.buaa.rental.Util.DataFormat;
import com.buaa.rental.Util.MyAnnotations.*;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/home")
public class HomeController
{

	@Autowired
	public ApplicationService applicationService;

	@Autowired
	HomeService homeService;

	@Authorization
	@PostMapping("rent")
	public CommonRep rent(@CurrentUser User user, @RequestBody @Valid Application app,
			BindingResult res)
			throws CodeException
	{
		if (res.hasErrors())
		{
			throw new CodeException("101", DataFormat.bindingErrorsFormat(res));
		}
		if (app.rentalmethod == null)
		{
			throw new CodeException("102", "租赁方式错误");
		}
		CommonRep rep = new CommonRep();
		Home home = homeService.findById(app.hid);
		if (home.roomStatus != RoomStatus.enable)
		{
			throw new CodeException("102", "此房屋不可申请");
		}
		app.uid = user.uid;
		app.status = Status.apply;
		app.contract = "";
		app = applicationService.insert(app);
		homeService.changeStatus(app.hid, RoomStatus.applied);
		rep.setData(app.id);
		return rep;
	}

	@Authorization
	@PostMapping("pay")
	public CommonRep pay(@CurrentUser User user, long aid) throws CodeException
	{
		Application app = applicationService.findById(aid);
		if (app.uid == user.uid && app.status == Status.pass
				&& app.rentalmethod == Rentalmethod.shortr)
		{
			applicationService.changeStatus(aid, Status.success);
			homeService.changeStatus(app.hid, RoomStatus.leased);
		} else
		{
			throw new CodeException("102", "参数错误");
		}
		return new CommonRep();
	}

	@Authorization
	@PostMapping("renewal")
	public CommonRep renewal(@CurrentUser User user, @RequestBody @Valid Application app,
			BindingResult res)
			throws CodeException
	{
		if (res.hasErrors())
		{
			throw new CodeException("101", DataFormat.bindingErrorsFormat(res));
		}
		CommonRep rep = new CommonRep();
		Application oldApp = applicationService.findById(app.id);
		Home home = homeService.findById(app.hid);
		if (home.roomStatus != RoomStatus.leased || oldApp.rentalmethod != Rentalmethod.longr)
		{
			throw new CodeException("102", "房屋不可续租");
		}
		if (app.status != Status.success)
		{
			throw new CodeException("102", "订单状态错误");
		}
		applicationService.changeStatus(app.id, Status.renewal);
		applicationService.updateRenewalTime(app.id, app.endTime);
		return rep;
	}


	@GetMapping(value = "search")
	public CommonRep Search(@RequestParam("kw") String pattern)
	{
		CommonRep rep = new CommonRep();
		rep.setData(homeService.searchHomes(pattern));
		return rep;
	}

	@GetMapping(value = "getHomeInfo/{hid}")
	public CommonRep getHomeInfo(@PathVariable("hid")long hid)
	{
		CommonRep rep = new CommonRep();
		Home h = homeService.findById(hid);
		rep.setData(h);
		return rep;
	}

}
