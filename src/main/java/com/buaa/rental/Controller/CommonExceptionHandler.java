package com.buaa.rental.Controller;

import com.buaa.rental.Dao.CommonRep;
import com.buaa.rental.Util.CodeException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class CommonExceptionHandler
{

	@ExceptionHandler(CodeException.class)
	@ResponseBody
	public CommonRep codeExceptionHandler(CodeException e)
	{
		CommonRep result = new CommonRep(e.code, e.getMessage());
		return result;

	}
	@ExceptionHandler(IllegalStateException.class)
	@ResponseBody
	public CommonRep illegalStateExceptionHandler(IllegalStateException e)
	{
		CommonRep result = new CommonRep("405", e.getMessage());
		return result;
	}
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public CommonRep exceptionHandler(Exception e)
	{
		CommonRep result = new CommonRep("121", e.getMessage());
		return result;

	}
}
