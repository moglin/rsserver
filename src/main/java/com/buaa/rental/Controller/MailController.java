package com.buaa.rental.Controller;

import com.buaa.rental.Dao.User;
import com.buaa.rental.Service.MailService;
import com.buaa.rental.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class MailController
{

	@Autowired
	public UserService userService;
	@Autowired
	private MailService mailService;

	@Scheduled(cron = " 0 0 8 25 * *")
	public void paymentNotify()
	{
		for (User u : userService.getUsers())
		{
			try
			{
				mailService.sendMessage("交房租", "交房租", u.email);
			} catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
