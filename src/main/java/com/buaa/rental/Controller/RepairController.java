package com.buaa.rental.Controller;

import com.buaa.rental.Dao.Application;
import com.buaa.rental.Dao.CommonRep;
import com.buaa.rental.Dao.Repair;
import com.buaa.rental.Dao.User;
import com.buaa.rental.Service.ApplicationService;
import com.buaa.rental.Service.HomeService;
import com.buaa.rental.Service.RepairService;
import com.buaa.rental.Service.UserService;
import com.buaa.rental.Util.CodeException;
import com.buaa.rental.Util.MyAnnotations.Authorization;
import com.buaa.rental.Util.MyAnnotations.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.buaa.rental.Dao.User.Role.user;

@RestController
@RequestMapping("api/repair")
public class RepairController {

    @Autowired
    RepairService repairService;
    @Autowired
    ApplicationService applicationService;
    @Autowired
    UserService userService;
    @Autowired
    HomeService homeService;

    @Authorization
    @RequestMapping("add")
    public CommonRep addRepair(@CurrentUser User user, @RequestBody Map<String, Object> map) throws Exception {
        long aid = Integer.parseInt((String) map.get("aid"));
        Repair.Message init = new Repair.Message();
        init.uid = user.uid;
        init.text = (String) map.get("text");
        String title = (String) map.get("title");
        try {
            init.photos = (List<String>) map.get("photos");
        } catch (ClassCastException ex) {
            throw new CodeException("102", "格式错误");
        }
        long id = repairService.addRepair(aid, init, title);
        CommonRep rep = new CommonRep();
        rep.setData(id);
        return rep;
    }

    @Authorization
    @RequestMapping("details")
    public CommonRep repairDetails(@CurrentUser User user, @RequestBody Map<String, String> map) throws Exception {
        long rid = Integer.parseInt(map.get("rid"));
        Repair repair = repairService.getRepairById(rid);
        CommonRep rep = new CommonRep();
        rep.setData(repair);
        return rep;
    }

    @Authorization
    @RequestMapping("getByOrder")
    public CommonRep allRepairs(@CurrentUser User user, @RequestBody Map<String, String> map) throws Exception {
        long aid = Integer.parseInt(map.get("aid"));
        if (applicationService.findById(aid).uid != user.uid) {
            throw new CodeException("102", "用户无权访问！");
        }
        List<Repair> repairs = repairService.getRepairByApplication(aid);
        CommonRep rep = new CommonRep();
        rep.setData(repairs);
        return rep;
    }

    @Authorization
    @RequestMapping("message")
    public CommonRep addMessage(@CurrentUser User user, @RequestBody Map<String, Object> map) throws Exception {
        long rid = Integer.parseInt((String) map.get("rid"));
        Repair repair = repairService.getRepairById(rid);
        if (applicationService.findById(repair.aid).uid != user.uid) {
            throw new CodeException("102", "用户无权访问！");
        }
        Repair.Message message = new Repair.Message();
        message.uid = user.uid;
        message.text = (String) map.get("text");
        message.photos = (List<String>) map.get("photos");
        repairService.addMessage(repair, message);
        return new CommonRep();
    }

    @Authorization
    @PostMapping("close")
    public CommonRep closeRepair(@CurrentUser User user, @RequestBody Map<String, String> map) throws Exception {
        long rid = Integer.parseInt(map.get("rid"));
        Repair repair = repairService.getRepairById(rid);
        if (applicationService.findById(repair.aid).uid != user.uid) {
            throw new CodeException("102", "用户无权访问！");
        }
        repairService.changeStatus(rid, Repair.RepairState.closed);
        return new CommonRep();
    }

    @Authorization
    @RequestMapping("getAll")
    public CommonRep getAllRepairs(@CurrentUser User user) {
        CommonRep rep = new CommonRep();
        rep.setData(repairService.getRepairByClient(user.uid));
        return rep;
    }

    @Authorization
    @PostMapping("getExtra")
    public CommonRep getExtra(@CurrentUser User user, @RequestBody Map<String, String> inmap) {
        long rid = Integer.parseInt(inmap.get("rid"));
        Map<String, String> map = new HashMap<>();
        Repair repair = repairService.getRepairById(rid);
        Application application = applicationService.findById(repair.aid);
        if (repair.workerId != 0) map.put("workerName", userService.getUserByUid(repair.workerId).name);
        else map.put("workerName", "");
        map.put("customerName", userService.getUserByUid(application.uid).name);
        map.put("houseName", homeService.findById(application.hid).title);
        CommonRep rep = new CommonRep();
        rep.setData(map);
        return rep;
    }

    @Authorization
    @PostMapping("rate")
    public CommonRep rate(@CurrentUser User user, @RequestBody Map<String, Object> map) throws CodeException {
        long rid = Integer.parseInt((String) map.get("rid"));
        Repair repair = repairService.getRepairById(rid);
        if (applicationService.findById(repair.aid).uid != user.uid) {
            throw new CodeException("102", "用户无权访问！");
        }
        Repair.Message message = new Repair.Message();
        message.uid = user.uid;
        message.text = (String) map.get("text");
        message.photos = (List<String>) map.get("photos");
        Repair.Opinion opinion = Repair.Opinion.valueOf((String) map.get("opinion"));
        repairService.rate(rid, message, opinion);
        return new CommonRep();
    }
}
